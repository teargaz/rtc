#include <Wire.h>
#include "RTClib.h"
#include <SPI.h>
//RTC_Millis rtc;
RTC_DS3231 rtc;
int houR = 0;
int minutE = 0;
int a = 2;
int b = 3;
int c = 4;
int d = 12; //5
int e = 13; //6
int f = 7;
int g = 8;
int DP = 9;
int panel[4] = {10, 11, 5, 6}; //10,11,12,13
int digit = 9;
int lagg = 1;
int panelDigit[4] = {0, 0, 0, 0};
int i = 0;
int j = 0;
//int dimPin = A0;
//int dimValue = 0;

void setup () {
  pinMode(g, OUTPUT);
  pinMode(f, OUTPUT);
  pinMode(a, OUTPUT);
  pinMode(b, OUTPUT);
  pinMode(d, OUTPUT);
  pinMode(c, OUTPUT);
  pinMode(DP, OUTPUT);
  pinMode(e, OUTPUT);
  pinMode(panel[0], OUTPUT);
  pinMode(panel[1], OUTPUT);
  pinMode(panel[2], OUTPUT);
  pinMode(panel[3], OUTPUT);
  analogReference(DEFAULT);
  Serial.begin(38400);
  // folHIGHing line sets the RTC to the date & time this sketch was compiled
  
  //rtc.begin(DateTime(F(__DATE__), F(__TIME__)));
  rtc.begin();
  // This line sets the RTC with an explicit date & time, for example to set
  // January 21, 2014 at 3am you would call:
  
   //rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
   //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
   Wire.begin();
}

void resetPanel() {
  digitalWrite(g, HIGH);
  digitalWrite(f, LOW);
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(d, LOW);
  digitalWrite(DP, LOW);
  digitalWrite(e, LOW);
  digitalWrite(c, LOW);
}

void loop () {
  DateTime now = rtc.now();
//  dimValue = analogRead(dimPin);
  //Serial.print("Unmapped :");
  //Serial.println(dimValue);

  //dimValue = map(dimValue, 0,1023,0,255);
  //Serial.print("Mapped :");
  //Serial.println(dimValue);
  


  // if (now.minute() != minutE) {
    Serial.print("Time right now: ");
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.println();
    //} 
  houR = now.hour();
  minutE = now.minute();
  panelDigit[2] = houR % 10;
  houR = houR / 10;
  panelDigit[3] = houR % 10;
  //valueNr = valueNr / 10;
  panelDigit[0] = minutE % 10;
  minutE = minutE / 10;
  panelDigit[1] = minutE % 10;

  Serial.print(panelDigit[0]);
  Serial.print(panelDigit[1]);
  Serial.print(panelDigit[2]);
  Serial.println(panelDigit[3]);
  i = 0;
  for (; i <= 3; i++) {
    analogWrite(panel[i], 0);
    delay(lagg);
    if (i > 0) {
      analogWrite(panel[i - 1], 255);
      delay(lagg);
    }
    //Serial.print(i);
    digit = panelDigit[3 - i];

    if (i == 1) {
      digitalWrite(DP, HIGH);
      delay(lagg);
    }

    if (digit == 0) {
      digitalWrite(g, LOW);
      digitalWrite(f, HIGH);
      digitalWrite(a, HIGH);
      digitalWrite(b, HIGH);
      digitalWrite(d, HIGH);
      digitalWrite(DP, LOW);
      digitalWrite(e, HIGH);
      digitalWrite(c, HIGH);
      delay(lagg);
      digitalWrite(g, LOW);
      digitalWrite(f, LOW);
      digitalWrite(a, LOW);
      digitalWrite(b, LOW);
      digitalWrite(d, LOW);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, LOW);
    } else if (digit == 1) {
      digitalWrite(g, LOW);
      digitalWrite(f, LOW);
      digitalWrite(a, LOW);
      digitalWrite(b, HIGH);
      digitalWrite(d, LOW);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, HIGH);
      delay(lagg);
      digitalWrite(g, LOW);
      digitalWrite(f, LOW);
      digitalWrite(a, LOW);
      digitalWrite(b, LOW);
      digitalWrite(d, LOW);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, LOW);
    } else if (digit == 2) {
      digitalWrite(g, HIGH);
      digitalWrite(f, LOW);
      digitalWrite(a, HIGH);
      digitalWrite(b, HIGH);
      digitalWrite(d, HIGH);
      digitalWrite(DP, LOW);
      digitalWrite(e, HIGH);
      digitalWrite(c, LOW);
      delay(lagg);
      digitalWrite(g, LOW);
      digitalWrite(f, LOW);
      digitalWrite(a, LOW);
      digitalWrite(b, LOW);
      digitalWrite(d, LOW);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, LOW);
    } else if (digit == 3) {
      digitalWrite(g, HIGH);
      digitalWrite(f, LOW);
      digitalWrite(a, HIGH);
      digitalWrite(b, HIGH);
      digitalWrite(d, HIGH);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, HIGH);
      delay(lagg);
      digitalWrite(g, LOW);
      digitalWrite(f, LOW);
      digitalWrite(a, LOW);
      digitalWrite(b, LOW);
      digitalWrite(d, LOW);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, LOW);
    } else if (digit == 4) {
      digitalWrite(g, HIGH);
      digitalWrite(f, HIGH);
      digitalWrite(a, LOW);
      digitalWrite(b, HIGH);
      digitalWrite(d, LOW);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, HIGH);
      delay(lagg);
      digitalWrite(g, LOW);
      digitalWrite(f, LOW);
      digitalWrite(a, LOW);
      digitalWrite(b, LOW);
      digitalWrite(d, LOW);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, LOW);
    } else if (digit == 5) {
      digitalWrite(g, HIGH);
      digitalWrite(f, HIGH);
      digitalWrite(a, HIGH);
      digitalWrite(b, LOW);
      digitalWrite(d, HIGH);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, HIGH);
      delay(lagg);
      digitalWrite(g, LOW);
      digitalWrite(f, LOW);
      digitalWrite(a, LOW);
      digitalWrite(b, LOW);
      digitalWrite(d, LOW);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, LOW);
    } else if (digit == 6) {
      digitalWrite(g, HIGH);
      digitalWrite(f, HIGH);
      digitalWrite(a, HIGH);
      digitalWrite(b, LOW);
      digitalWrite(d, HIGH);
      digitalWrite(DP, LOW);
      digitalWrite(e, HIGH);
      digitalWrite(c, HIGH);
      delay(lagg);
      digitalWrite(g, LOW);
      digitalWrite(f, LOW);
      digitalWrite(a, LOW);
      digitalWrite(b, LOW);
      digitalWrite(d, LOW);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, LOW);
    } else if (digit == 7) {
      digitalWrite(g, LOW);
      digitalWrite(f, LOW);
      digitalWrite(a, HIGH);
      digitalWrite(b, HIGH);
      digitalWrite(d, LOW);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, HIGH);
      delay(lagg);
      digitalWrite(g, LOW);
      digitalWrite(f, LOW);
      digitalWrite(a, LOW);
      digitalWrite(b, LOW);
      digitalWrite(d, LOW);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, LOW);
    } else if (digit == 8) {
      digitalWrite(g, HIGH);
      digitalWrite(f, HIGH);
      digitalWrite(a, HIGH);
      digitalWrite(b, HIGH);
      digitalWrite(d, HIGH);
      digitalWrite(DP, LOW);
      digitalWrite(e, HIGH);
      digitalWrite(c, HIGH);
      delay(lagg);
      digitalWrite(g, LOW);
      digitalWrite(f, LOW);
      digitalWrite(a, LOW);
      digitalWrite(b, LOW);
      digitalWrite(d, LOW);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, LOW);
    } else if (digit == 9) {
      digitalWrite(g, HIGH);
      digitalWrite(f, HIGH);
      digitalWrite(a, HIGH);
      digitalWrite(b, HIGH);
      digitalWrite(d, HIGH);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, HIGH);
      delay(lagg);
      digitalWrite(g, LOW);
      digitalWrite(f, LOW);
      digitalWrite(a, LOW);
      digitalWrite(b, LOW);
      digitalWrite(d, LOW);
      digitalWrite(DP, LOW);
      digitalWrite(e, LOW);
      digitalWrite(c, LOW);
    }
    digitalWrite(g, LOW);
    digitalWrite(f, LOW);
    digitalWrite(a, LOW);
    digitalWrite(b, LOW);
    digitalWrite(d, LOW);
    digitalWrite(DP, LOW);
    digitalWrite(e, LOW);
    digitalWrite(c, LOW);
  } // Closes i Loop
  delay(lagg);
  digitalWrite(panel[3], HIGH);
}
